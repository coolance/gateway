package com.example.gateway;

import lombok.Data;

import java.time.LocalTime;

/**
 * @author Coolance
 * @description
 * @date 2022-03-10
 */
@Data
public class TimeBetweenConfig {

    private LocalTime start;

    private LocalTime end;

}
